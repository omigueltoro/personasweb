import { TipoDocumento } from './TipoDocumento';
import { Pais } from './Pais';

export class Persona{
    id:number;
    nombres:string;
    apellidos:string;
    tipoDocumento: TipoDocumento;
    tipoDocumentoCol: string;
    documento: string;
    genero: string;
    edad: number;
    pais: Pais;
    paisCol: string;
    
    constructor(id:number = null,nombres:string = null,apellidos:string = null,tipoDocumento:TipoDocumento=null,tipoDocumentoCol:string = null,
                documento:string = null, genero:string = null,edad: number = null, pais: Pais = null, paisCol: string = null){
        
    }
}