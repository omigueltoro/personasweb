import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'personasweb';
  items1: MenuItem[];

  ngOnInit(){
    
    this.items1 = [
      {label: 'Tipos de documento', icon: 'pi pi-fw pi-home', routerLink: 'tipos-documento'},
      {label: 'Paises', icon: 'pi pi-fw pi-pencil', routerLink: 'paises'},
      {label: 'Personas', icon: 'pi pi-fw pi-file', routerLink: 'personas'}
  ];

  }
}
