import { Component, OnInit } from '@angular/core';
import { Persona } from 'src/models/Persona';
import { MenuItem, ConfirmationService, SelectItem } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { Pais } from 'src/models/Pais';
import { TipoDocumento } from 'src/models/TipoDocumento';
import { PersonasService } from 'src/app/services/personas/personas.service';
import { PaisesService } from 'src/app/services/paises/paises.service';
import { TiposDocumentoService } from 'src/app/services/tipos-documento/tipos-documento.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {

  public title: string = "Personas";
  personas: Persona[];
  cols: any[];
  items: MenuItem[];
  displayPersonaSaveDialog: boolean = false;
  persona: Persona = {
    id: null,
    nombres: null,
    apellidos: null,
    tipoDocumento: null,
    tipoDocumentoCol: null,
    documento: null,
    genero: null,
    edad: null,
    pais: null,
    paisCol: null
  };
  selectedPersona: Persona = {
    id: null,
    nombres: null,
    apellidos: null,
    tipoDocumento: null,
    tipoDocumentoCol: null,
    documento: null,
    genero: null,
    edad: null,
    pais: null,
    paisCol: null
  };

  public tiposDocumento: SelectItem[];
  public paises: SelectItem[];

  constructor(private PersonaService: PersonasService, private messageService: MessageService, private confirmService: ConfirmationService,
    private paisesService: PaisesService, private tiposDocumentoService: TiposDocumentoService) { }

  ngOnInit(): void {
    var that = this;

    this.getAll();
    this.cols = [
      { field: "id", header: "Item" },
      { field: "nombres", header: "Nombres" },
      { field: "apellidos", header: "Apellidos" },
      { field: "tipoDocumentoCol", header: "Tipo de documento" },
      { field: "documento", header: "Documento" },
      { field: "genero", header: "Genero" },
      { field: "edad", header: "Edad" },
      { field: "paisCol", header: "Pais" },
    ];
    this.items = [
      {
        label: "Nuevo",
        icon: "pi pi-fw pi-plus",
        command: () => this.showSaveDialog(false)
      },
      {
        label: "Editar",
        icon: "pi pi-fw pi-pencil",
        command: () => this.selectedPersona.id != null ? this.showSaveDialog(true) : this.messageService.add({ severity: 'warn', summary: "Resultado", detail: "Item not selected" })
      },
      {
        label: "Eliminar",
        icon: "pi pi-fw pi-times",
        command: () => this.delete()
      }
    ];
    this.paisesService.getAll().subscribe(
      (result: Pais[]) => {
        if (result) {
          let selectItem: SelectItem [] = []
          for(let i = 0; i < result.length; i++){
            
            let item: SelectItem = { label: result[i].nombre, value: result[i] }
            selectItem.push(item);
          }
          this.paises = selectItem;
        }
      },
      error => {
        console.log(error);
      }
    );
    this.tiposDocumentoService.getAll().subscribe(
      (result: TipoDocumento[]) => {
        if (result) {
          let selectItem: SelectItem [] = []
          for(let i = 0; i < result.length; i++){
            
            let item: SelectItem = { label: result[i].nombre, value: result[i] }
            selectItem.push(item);
          }
          this.tiposDocumento = selectItem;
        }
      },
      error => {
        console.log(error);

      }
    );

  }

  getAll() {
    this.PersonaService.getAll().subscribe(
      (result: any) => {
        let personas: any[] = [];
        for (let i = 0; i < result.length; i++) {
          let persona = result[i];
          persona.tipoDocumentoCol = result[i].tipoDocumento.nombre;
          persona.paisCol = result[i].pais.nombre;
          personas.push(persona);
        }
        this.personas = personas;
      },
      error => {
        console.log(error);
      }

    )
  }
  showSaveDialog(editar: boolean): void {
    if (editar) {
      this.persona = this.selectedPersona;
    }
    else {
      this.persona = {
        id: null,
        nombres: null,
        apellidos: null,
        tipoDocumento: null,
        tipoDocumentoCol: null,
        documento: null,
        genero: null,
        edad: null,
        pais: null,
        paisCol: null
      };
    }
    this.displayPersonaSaveDialog = true;
  }
  save() {
    this.PersonaService.save(this.persona).subscribe(
      (result: any) => {
        
        let persona : Persona = result;
        persona.tipoDocumentoCol = result.tipoDocumento.nombre;
        persona.paisCol = result.pais.nombre;
        let index = this.personas.findIndex(p => p.id == persona.id);
        index != -1 ? this.personas[index] = persona : this.personas.push(persona);
        this.messageService.add({ severity: 'success', summary: "Resultado", detail: "Transacción exitosa" })
        this.displayPersonaSaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete() {
    if (this.selectedPersona == null || this.selectedPersona.id == null) {
      this.messageService.add({ severity: 'warn', summary: "Resultado", detail: "Item not selected" })
      return;
    }
    this.confirmService.confirm({
      message: "¿Está seguro que desea eliminar este registro?",
      accept: () => {
        this.PersonaService.delete(this.selectedPersona.id).subscribe(
          (result: any) => {
            this.messageService.add({ severity: 'success', summary: "Resultado", detail: "Transacción exitosa" })
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject(id: number) {
    let index = this.personas.findIndex(p => p.id == id);
    if (index != -1) {
      this.personas.splice(index, 1)

    }
  }
  onSelectPais(pais: any){
    this.persona.pais = pais;
  }
  onSelectTipoDocumento(tipoDocumento: any){
    this.persona.tipoDocumento = tipoDocumento;
  }
}
