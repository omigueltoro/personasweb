import { Component, OnInit } from '@angular/core';
import { Pais } from 'src/models/Pais';
import { MenuItem, ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { PaisesService } from 'src/app/services/paises/paises.service';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.css'],
})
export class PaisesComponent implements OnInit {
  
  public title : string = "Paises";
  paises : Pais[];
  cols : any [];
  items: MenuItem [];
  displayPaisSaveDialog : boolean = false;
  pais : Pais = {
    id: null,
    nombre : null
  };
  selectedDamageDepthLevel: Pais = {
    id: null,
    nombre : null
  }

  constructor(private paisesService: PaisesService, private messageService: MessageService, private confirmService : ConfirmationService) { }

  ngOnInit(): void {
    this.getAll();
    this.cols = [
      {field: "id", header: "Item"},
      {field: "nombre", header: "Pais"}
    ];
    this.items = [
      {
        label : "Nuevo",
        icon : "pi pi-fw pi-plus",
        command : () => this.showSaveDialog(false)
      },
      {
        label : "Editar",
        icon : "pi pi-fw pi-pencil",
        command : () => this.selectedDamageDepthLevel.id !=null ? this.showSaveDialog(true) : this.messageService.add({severity: 'warn', summary: "Resultado", detail: "Item not selected"})
      },
      {
        label : "Eliminar",
        icon : "pi pi-fw pi-times",
        command : () => this.delete()
      }
    ]
  }

  getAll(){
    this.paisesService.getAll().subscribe(
      (result: any) => {
        let paises : Pais[] = [];
        for(let i =0; i < result.length; i++){
          let pais = result[i] as Pais;
          paises.push(pais);
        }
        this.paises = paises;
      },
      error => {
        console.log(error);
      }
      
    )
  }
  showSaveDialog(editar:boolean): void {
    if(editar){
      this.pais = this.selectedDamageDepthLevel;
    }
    else{
      this.pais = {
        id: null,
        nombre : null
      };
    }
    this.displayPaisSaveDialog = true;
  }
  save(){
    this.paisesService.save(this.pais).subscribe(
      (result:any) => {
        let pais = result as Pais;
        let index = this.paises.findIndex(p => p.id == pais.id);
        index != -1 ? this.paises[index] = pais : this.paises.push(pais);
        this.messageService.add({severity: 'success', summary: "Resultado", detail: "Transacción exitosa"})
        this.displayPaisSaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete(){
    if(this.selectedDamageDepthLevel == null || this.selectedDamageDepthLevel.id == null){
      this.messageService.add({severity: 'warn', summary: "Resultado", detail: "Item not selected"})
      return;
    }
    this.confirmService.confirm({
      message: "¿Está seguro que desea eliminar este registro?",
      accept : () => {
        this.paisesService.delete(this.selectedDamageDepthLevel.id).subscribe(
          (result : any) => {
            this.messageService.add({severity: 'success', summary: "Resultado", detail: "Transacción exitosa"})
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject (id:number) {
    let index = this.paises.findIndex(p => p.id == id);
    if(index != -1){
      this.paises.splice(index, 1)

    }
  }
}
