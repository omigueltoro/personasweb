import { Component, OnInit } from '@angular/core';
import { TipoDocumento } from 'src/models/TipoDocumento';
import { MenuItem, ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { TiposDocumentoService } from 'src/app/services/tipos-documento/tipos-documento.service';

@Component({
  selector: 'app-tiposDocumento',
  templateUrl: './tipos-documento.component.html',
  styleUrls: ['./tipos-documento.component.css'],
})
export class TiposDocumentoComponent implements OnInit {
  
  public title : string = "Tipos de documento";
  tiposDocumento : TipoDocumento[];
  cols : any [];
  items: MenuItem [];
  displaySaveDialog : boolean = false;
  tipoDocumento : TipoDocumento = {
    id: null,
    nombre: null,
    descripcion : null
  };
  selectedTipoDocumento: TipoDocumento = {
    id: null,
    nombre: null,
    descripcion : null
  }

  constructor(private paintingTypesService: TiposDocumentoService, private messageService: MessageService, private confirmService : ConfirmationService) { }

  ngOnInit(): void {
    this.getAll();
    this.cols = [
      {field: "id", header: "Item"},
      {field: "nombre", header: "Tipo de documento"},
      {field: "descripcion", header: "Description"},
    ];
    this.items = [
      {
        label : "Nuevo",
        icon : "pi pi-fw pi-plus",
        command : () => this.showSaveDialog(false)
      },
      {
        label : "Editar",
        icon : "pi pi-fw pi-pencil",
        command : () => this.selectedTipoDocumento.id !=null ? this.showSaveDialog(true) : this.messageService.add({severity: 'warn', summary: "Resultado", detail: "Item not selected"})
      },
      {
        label : "Eliminar",
        icon : "pi pi-fw pi-times",
        command : () => this.delete()
      }
    ]
  }

  getAll(){
    this.paintingTypesService.getAll().subscribe(
      (result: any) => {
        let tiposDocumento : TipoDocumento[] = [];
        for(let i =0; i < result.length; i++){
          let tipoDocumento = result[i] as TipoDocumento;
          tiposDocumento.push(tipoDocumento);
        }
        this.tiposDocumento = tiposDocumento;
      },
      error => {
        console.log(error);
      }
      
    )
  }
  showSaveDialog(editar:boolean): void {
    if(editar){
      this.tipoDocumento = this.selectedTipoDocumento;
    }
    else{
      this.tipoDocumento = {
        id: null,
        nombre: null,
        descripcion : null
      };
    }
    this.displaySaveDialog = true;
  }
  save(){
    this.paintingTypesService.save(this.tipoDocumento).subscribe(
      (result:any) => {
        let tipoDocumento = result as TipoDocumento;
        let index = this.tiposDocumento.findIndex(p => p.id == tipoDocumento.id);
        index != -1 ? this.tiposDocumento[index] = tipoDocumento : this.tiposDocumento.push(tipoDocumento);
        this.messageService.add({severity: 'success', summary: "Resultado", detail: "Transacción exitosa"})
        this.displaySaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete(){
    if(this.selectedTipoDocumento == null || this.selectedTipoDocumento.id == null){
      this.messageService.add({severity: 'warn', summary: "Resultado", detail: "Item not selected"})
      return;
    }
    this.confirmService.confirm({
      message: "¿Está seguro que desea eliminar este registro?",
      accept : () => {
        this.paintingTypesService.delete(this.selectedTipoDocumento.id).subscribe(
          (result : any) => {
            this.messageService.add({severity: 'success', summary: "Resultado", detail: "Transacción exitosa"})
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject (id:number) {
    let index = this.tiposDocumento.findIndex(p => p.id == id);
    if(index != -1){
      this.tiposDocumento.splice(index, 1)

    }
  }
}
