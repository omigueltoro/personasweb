import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaintingtypesComponent } from './tipos-documento.component';

describe('PaintingtypesComponent', () => {
  let component: PaintingtypesComponent;
  let fixture: ComponentFixture<PaintingtypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaintingtypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaintingtypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
