import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Persona } from 'src/models/Persona';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  baseUrl:string = "http://localhost:8080/api/v1"

  constructor(private http:HttpClient) { }

  getAll() : Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
      return this.http.get(this.baseUrl+"/personas", {headers : headers});
  }
  save(personas: Persona): Observable<any> {
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.baseUrl+"/personas", JSON.stringify(personas), {headers : headers});
  }
  delete(id:number) : Observable<any>{

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.delete(this.baseUrl+"/personas/"+id, {headers : headers});
  }
}
