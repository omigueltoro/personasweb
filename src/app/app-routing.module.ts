import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TiposDocumentoComponent } from './components/tipos-documento/tipos-documento.component';
import { PaisesComponent } from './components/paises/paises.component';
import { PersonasComponent } from './components/personas/personas.component';

const routes: Routes = [
  {
    path: 'tipos-documento',
    component: TiposDocumentoComponent
  },
  {
    path: 'paises',
    component: PaisesComponent
  },
  {
    path: 'personas',
    component: PersonasComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
